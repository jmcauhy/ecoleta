<div align="center">
    <img alt="Ecoleta" title="Ecoleta" src="frontend/src/assets/logo.svg" width="220px" />
</div>
<br/>
<div align="center">A project developed during the 1st NLW, by [Rocketseat](https://github.com/rocketseat/)</div>
<br/>
<hr/>
<br/>
<div align="center">
    <img alt="Ecoleta Home" src="https://i.imgur.com/wsLZKcM.png" />
    <img alt="Ecoleta Registro" src="https://i.imgur.com/P7oo1QI.png" />
</div>

## :clipboard: Description

Ecoleta is a marketplace developed to give users a way to know the places around them that collect electronic components and/or residues for disposal.

## :computer: Technologies
This project was developed using [React](https://reactjs.org/) with [Axios](https://github.com/axios/axios), 
[Node.js](https://nodejs.org/en/) with [Express](https://expressjs.com/pt-br/) and [SQLite](https://www.sqlite.org/) with [Knex.js](http://knexjs.org/).

## :bulb: How to contribute

- Fork this repository;
- Create a branch with your feature: `git checkout -b my-feature`;
- Commit your changes: `git commit -m 'My new feature'`;
- Push to your branch: `git push origin my-feature`.

## :scroll: License

This project is under the [MIT](https://choosealicense.com/licenses/mit/) license.