import React, { useState, useEffect, ChangeEvent, FormEvent } from 'react'
import { Link, useHistory } from 'react-router-dom'
import { Map, TileLayer, Marker } from 'react-leaflet'
import { LeafletMouseEvent } from 'leaflet'
import { FiArrowLeft } from 'react-icons/fi'

import api from '../../services/api'
import axios from 'axios'
import './styles.css'

import logo from '../../assets/logo.svg'

interface Item {
   id: number
   title: string
   image_url: string
}

interface IBGEUFResponse { sigla: string }

interface IBGECityResponse { nome: string }

const CreatePoint: React.FC = () => {
   const [items, setItems] = useState<Item[]>([]),
         [UFs, setUFs] = useState<string[]>([]),
         [cities, setCities] = useState<string[]>([])

   const [initialMapPosition, setInicialMapPosition] = useState<[number, number]>([0, 0])

   const [inputData, setInputData] = useState({ name: '', email: '', whatsapp: '' })
   
   const [selectedUF, setSelectedUF] = useState('0'),
         [selectedCity, setSelectedCity] = useState(''),
         [selectedMapPosition, setSelectedMapPosition] = useState<[number, number]>([0, 0]),
         [selectedItems, setSelectedItems] = useState<number[]>([])

   const history = useHistory()

   useEffect(() => {( async () => {
      const { data } = await api.get('/items')

      setItems(data)
   })()}, [])

   useEffect(() => {
      navigator.geolocation.getCurrentPosition(position => {
         const { latitude, longitude } = position.coords
         
         setInicialMapPosition([latitude, longitude])
      })
   }, [])

   useEffect(() => {( async () => {
      const { data } = await axios.get<IBGEUFResponse[]>
      ('https://servicodados.ibge.gov.br/api/v1/localidades/estados')

      const filteredData = data.map(uf => uf.sigla)

      setUFs(filteredData)
   })()}, [])

   useEffect(() => {( async () => {
      if (!selectedUF) return

      const { data } = await axios.get<IBGECityResponse[]>
      (`https://servicodados.ibge.gov.br/api/v1/localidades/estados/${selectedUF}/municipios`)

      const filteredData = data.map(city => city.nome)

      setCities(filteredData)
   })()}, [selectedUF])

   const handleSelectUF = (e: ChangeEvent<HTMLSelectElement>) => {
      const UF = e.target.value

      setSelectedUF(UF)
   }

   const handleSelectCity = (e: ChangeEvent<HTMLSelectElement>) => {
      const city = e.target.value

      setSelectedCity(city)
   }

   const handleMapClick = (e: LeafletMouseEvent) => {
      setSelectedMapPosition([ e.latlng.lat, e.latlng.lng ])
   }

   const handleInputChange = (e: ChangeEvent<HTMLInputElement>) => {
      const { name, value } = e.target

      setInputData({ ...inputData, [name]: value })
   }

   const handleSelectItem = (id: number) => {
      const alreadySelected = selectedItems.findIndex(item => item === id)

      if (alreadySelected >= 0) {
         const filteredItems = selectedItems.filter(item => item !== id)

         return setSelectedItems(filteredItems)
      }

      setSelectedItems([ ...selectedItems, id ])
   }

   const handleSubmit = async (e: FormEvent) => {
      e.preventDefault()

      try {
         const { name, email, whatsapp } = inputData,
         uf = selectedUF,
         city = selectedCity,
         [latitude, longitude] = selectedMapPosition,
         items = selectedItems

         const data = { name, email, whatsapp, uf, city, latitude, longitude, items }

         await api.post('/points', data) && alert('Ponto de coleta criado com sucesso!')

         history.push('/')
      }
      catch(error) {
         console.log(error)
      }
   }

   return (
      <div id="page-create-point">
         <header>
            <img src={logo} alt="Ecoleta" />

            <Link to="/">
               <FiArrowLeft />
               Voltar para Home
            </Link>
         </header>

         <form onSubmit={handleSubmit}>
            <h1>Cadastro do ponto de coleta</h1>

            <fieldset>
               <legend><h2>Dados</h2></legend>

               <div className="field">
                  <label htmlFor="name">Nome da entidade</label>
                  <input type="text" name="name" id="name" onChange={handleInputChange} />
               </div>

               <div className="field-group">
                  <div className="field">
                     <label htmlFor="email">E-mail</label>
                     <input type="email" name="email" id="email" onChange={handleInputChange} />
                  </div>

                  <div className="field">
                     <label htmlFor="whatsapp">WhatsApp</label>
                     <input type="text" name="whatsapp" id="whatsapp" onChange={handleInputChange} />
                  </div>
               </div>
            </fieldset>

            <fieldset>
               <legend>
                  <h2>Endereço</h2>
                  <span>Selecione o endereço no mapa</span>
               </legend>

               <Map center={initialMapPosition} zoom={15} onClick={handleMapClick}>
                  <TileLayer
                     attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                     url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                  />

                  <Marker position={selectedMapPosition} />
               </Map>

               <div className="field-group">
                  <div className="field">
                     <label htmlFor="uf">Estado (UF)</label>
                     <select name="uf" id="uf" value={selectedUF} onChange={handleSelectUF}>
                        <option>Selecione uma UF</option>
                        {UFs.map(uf => <option key={uf} value={uf}>{uf}</option>)}
                     </select>
                  </div>

                  <div className="field">
                     <label htmlFor="city">Cidade</label>
                     <select name="city" id="city" value={selectedCity} onChange={handleSelectCity}>
                        <option>Selecione uma cidade</option>
                        {cities.map(city => <option key={city} value={city}>{city}</option>)}
                     </select>
                  </div>
               </div>
            </fieldset>

            <fieldset>
               <legend>
                  <h2>Ítens de coleta</h2>
                  <span>Selecione um ou mais itens abaixo</span>
               </legend>

               <ul className="items-grid">
                  {items.map(item => 
                     <li
                        key={item.id}
                        className={selectedItems.includes(item.id) ? 'selected' : ''}
                        onClick={() => handleSelectItem(item.id)}
                     >
                        <img src={item.image_url} alt={item.title} />
                        <span>{item.title}</span>
                     </li>
                  )}
               </ul>
            </fieldset>

            <button type="submit">Cadastrar ponto de coleta</button>
         </form>
      </div>
   )
}

export default CreatePoint